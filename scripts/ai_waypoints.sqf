#include "\AC\defines\commonDefines.inc"

AC_battlesInProgress = [];

ACF_ai_move = {
	params ["_group","_pos",["_mode",B_DEFAULT],"_canGetOrders",["_radius",0]];
	if (_mode != B_DEFAULT) then {
		[_group,_mode] call ACF_ai_changeBehavior;
	};

	// Delete all waypoints
	while {count waypoints _group > 0} do {
		deleteWaypoint ((waypoints _group)#0);
	};

	if (!isNil "_canGetOrders") then {
		SVAR(_group,"canGetOrders",_canGetOrders);
	};

	//SVAR(_group,"#wp",_pos);
	private _wp = _group addWaypoint [_pos, _radius];
	_wp
};

ACF_rtbPos = {
	params ["_group"];
	private _side = side _group;
	private _pos = getPosWorld leader _group;

	//
	private _friendlyBases = AC_bases select {GVAR(_x,"side") == _side};
	_friendlyBases = [_friendlyBases,[],{_x distance2D _pos},"ASCEND"] call BIS_fnc_sortBy;
	pos = getPosWorld (_friendlyBases#0);
	_pos
};

ACF_ai_changeBehavior = {
	params ["_group","_behavior"];
	private _units = units _group;

	// Set specific behaviour parts
	switch (_behavior) do {
		case B_COMBAT: {
			_group setCombatMode "RED";
			_group setCombatBehaviour "COMBAT";
			_group setSpeedMode "FULL";
			{_x enableAI "autocombat"} forEach _units;
			//{_x enableAI "autotarget"} forEach _units;
			//{_x enableAI "cover"} forEach _units;
		};
		case B_TRANSPORT: {
			_group setCombatMode "YELLOW";
			_group setCombatBehaviour "SAFE"; //encourages road use, will switch to AWARE automatically if engaged
			_group setSpeedMode "NORMAL";
			{_x disableAI "autocombat"} forEach _units;
			//{_x disableAI "autotarget"} forEach _units;
			//{_x disableAI "cover"} forEach _units;
		};
		case B_SILENT: {
			_group setCombatMode "GREEN";
			_group setCombatBehaviour "STEALTH";
			_group setSpeedMode "LIMITED";
			{_x disableAI "autocombat"} forEach _units;
			//{_x disableAI "autotarget"} forEach _units;
			//{_x enableAI "cover"} forEach _units;
		};
	};

	// Apply special rules for helicopters
	if ([_group] call ACF_grp_isAircraft) then {
		{_x disableAI "autocombat"} forEach _units;

		private _multiplier = 1;
		if (_behavior == B_SILENT) then {_multiplier = 0.6};
		if (_behavior == B_TRANSPORT) then {_multiplier = 1.3};

		{
			[_x,SPEED_LIMIT_AIR * _multiplier] remoteExecCall ["limitSpeed",2];
			[_x,HELI_ALT * _multiplier] remoteExecCall ["flyInHeight",2];
		} forEach ([_group] call ACF_getGroupVehicles);
	};

	SVAR(_group,"#b",_behavior);
};

// TODO: Make the function more universal
AC_ai_fireMission = {
	params ["_group","_pos",["_spread",50],["_nRounds",4],["_sender",objNull]];
	private _artillery = vehicle leader _group;
	private _types = (getArtilleryAmmo [_artillery]);
	if (count _types == 0) exitWith {playSound "3DEN_notificationWarning"};
	private _roundType = _types#0;

	if (_pos isEqualType objNull) then {
		_pos = getPosWorld _pos;
	};
	private _roundPos = [_pos, _spread] call ACF_randomPos;

	// Check if target can be hit
	private _eta = _artillery getArtilleryETA [_roundPos, (getArtilleryAmmo [_artillery])#0];
	if (_eta == -1) exitWith {playSound "3DEN_notificationWarning"};

	// Create simulation zone, a small one
	doStop _artillery;
	private _simulationZone = (group ((entities "logic") select 0)) createUnit ["LOGIC",_pos , [], 0, ""];
	_simulationZone setVariable ["simulationRange", 300];
	AC_battlesInProgress pushBack _simulationZone;

	if (!isNull _sender) then {
		if (_eta > -1) then {
			[_eta,_pos,_spread + 15] remoteExec ["ACF_showFireZone", _sender];
		} else {
			"3DEN_notificationWarning" remoteExec ["playSound",_sender];
		};
	};
	_artillery commandArtilleryFire [_roundPos, _roundType, 4];
	sleep (_eta + 10);
	AC_battlesInProgress = AC_battlesInProgress - [_simulationZone];
	deleteVehicle _simulationZone;
};

ACF_showFireZone = {
	params ["_eta","_pos","_spread"];

	// Prepare marker
	playSound "FD_Finish_F";
	private _mrk = createMarkerLocal [str _pos,_pos];
	_mrk setMarkerShapeLocal "ELLIPSE";
	_mrk setMarkerSizeLocal [_spread,_spread];
	_mrk setMarkerColorLocal "colorBlack";
	_mrk setMarkerColorLocal "colorBlack";
	_mrk setMarkerAlphaLocal 0.5;
	sleep (_eta + 10);
	deleteMarkerLocal _mrk;
};

ACF_wp_getIn = {
	params ["_group","_target",["_auto",false]];
	deleteWaypoint [_group, 0];

	// Target must be position or object
	if (_target isEqualType grpNull) then {
		_target = vehicle leader _target;
	};


	{
		[_x] orderGetIn true;
	} forEach units _group;

	// Add or edit waypoint
	deleteWaypoint [_group, 0];
	private _wp = _group addWaypoint [getPosWorld _target, 0,0];

	if (_auto) then {
		_wp setWaypointType "GETIN NEAREST";
	} else {
		_wp waypointAttachVehicle _target;
		_wp setWaypointType "GETIN";

		private _targetGroup = group effectiveCommander _target;
		if ([_targetGroup] call ACF_grp_isAircraft) then {
			// Order aircraft to get to the group and land nearby
			deleteWaypoint [_targetGroup, 0];
			//[_targetGroup, getPos leader _group, objNull] spawn BIS_fnc_wpLand;
			[_group, getPosWorld leader _group, objNull] remoteExec ["BIS_fnc_wpLand",2];
		};

		// Track the vehicle
		[_group, _target] spawn {
			params ["_group","_target"];

			while {alive _target && waypointType [_group,0] == "GETIN"} do {
				sleep 1;
				[_group,0] setWPPos getPosWorld _target;
			};
		};
	};
};

ACF_wp_unload = {
	params ["_group"];

	private _transportedGroups = [_group] call ACF_ui_getTransportedGroups;
	// Remove all waypoints
	{
		while {count waypoints _x > 0} do {
			deleteWaypoint ((waypoints _x)#0);
		};
	} forEach (_transportedGroups + [_group]);


	_transportedGroups = _transportedGroups - [_group];

	{
		// Create get out waypoint for all groups
		private _wp = _x addWaypoint [getPosWorld leader _x, 0];
		_wp setWaypointType "GETOUT";

		{
			// TODO: Test it in MP, I might need to remoteExec it!
			[_x] orderGetIn false;
		    _x leaveVehicle vehicle _x;
		    unassignVehicle _x;
			commandGetOut _x;
		} forEach units _x;
	} forEach _transportedGroups;
};

ACF_wp_getOut = {
	params ["_group"];
	deleteWaypoint [_group, 0];

	// Target must be position or object
	if (_target isEqualType grpNull) then {
		_target = vehicle leader _target;
	};

	deleteWaypoint [_group, 1];
	private _wp = _group addWaypoint [getPos vehicle leader _group, 0,1];
	_wp setWaypointType "GETOUT";
};

