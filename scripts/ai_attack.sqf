#include "\AC\defines\commonDefines.inc"

ACF_ai_assignAttacks = {
	params ["_battalion"];
	private _side = GVAR(_battalion,"side");

	//private _enemySide = [_side] call ACF_enemySide;
	private _ongoingAttacks = GVARS(_battalion,"attacks",[]);

	//Get availible non-artillery units
	private _hqGroup = GVARS(_battalion,"hqElement",grpNull);
	private _groups = AC_operationGroups select {
		side _x == _side
		&& {GVARS(_x,"canGetOrders",true)}
		&& {_x != _hqGroup}
		&& {GVAR(_x,"type") != TYPE_ARTILLERY}
	};

	//Get a list of frontier bases
	private _borderBases = [_side] call ACF_borderBases;

	// Attack empty base if there is any
	private _emptyBases = _borderBases select {GVAR(_x,"side") == sideEmpty};
	_emptyBases = _emptyBases - _ongoingAttacks;

	// Attack empty base and end function
	if (count _emptyBases > 0 && count _groups > 0) exitWith {
		//get base near troops
		private _leadGroup = _groups#0;
		_emptyBases = [_emptyBases,[],{_x distance2D leader _leadGroup},"ASCEND"] call BIS_fnc_sortBy;
		private _emptyBase = _emptyBases#0;
		private _closestGroups = [_groups,[],{leader _x distance2D _emptyBase},"ASCEND"] call BIS_fnc_sortBy;
		private _closestGroup = _closestGroups#0;

		private _attacks = GVARS(_battalion,"attacks",[]);
		_attacks pushBack _emptyBase;
		SVAR(_battalion,"attacks", _attacks);

		//systemChat str ["Attacking empty base", GVAR(_emptyBase,"callsign")];
		[_emptyBase, [_closestGroup],_side] call ACF_ai_createOffensive;
	};

	//Get a list of enemy frontier bases
	private _targetBases = _borderBases select {GVAR(_x,"side") != _side};
	_targetBases = _targetBases - _ongoingAttacks;

	// Attack occupied base
	if (count _ongoingAttacks < 4 && count _targetBases > 0) then {
		_targetBases = [_targetBases,[],{GVAR(_x,"att_costDet") * ((_x distance2D _battalion) /1000) - (GVAR(_x,"thr_currentStr") * 1)},"ASCEND"] call BIS_fnc_sortBy;
		private _targetBase = _targetBases#0;
		_groups = [_groups,[],{leader _x distance2D _targetBase},"ASCEND"] call BIS_fnc_sortBy;

		// Send adequate strength to attack:
		private _strength = 0;
		private _requiredStrength = (GVAR(_targetBase,"att_costDet")) max 9;

		{
			_strength = _strength + ([_x] call ACF_ai_groupStrength);

			// Create attack only if enough soldiers are available
			if (_strength >= _requiredStrength) exitWith {
				_groups resize (_forEachIndex + 1);
				//systemChat str ["Creating attack",_strength, _requiredStrength, _groups apply {GVAR(_x,"callsign")}];
				[_targetBase, _groups,_side] call ACF_ai_createOffensive;
				private _attacks = GVARS(_battalion,"attacks",[]);
				_attacks pushBack _targetBase;
				SVAR(_battalion,"attacks", _attacks);
				//systemChat str (_attacks apply {_x getVariable "callsign"});
			};

		} forEach _groups;

		sleep STRATEGY_TICK;
		if !(IS_AI_ENABLED(_battalion)) exitWith {};
		//determine defender detection type
		private _color = switch (_side) do {
		    case west: { "Wdetected" };
		    case east: { "Edetected" };
		    case resistance: { "Idetected" };
		};
		private _enemyGroups = AC_operationGroups select {side _x != _side && {GVARS(_x,_color,false)} };

		if (count _enemyGroups > 0) then {
			_enemyGroups = [_enemyGroups,[],{leader _x distance2D _targetBase},"ASCEND"] call BIS_fnc_sortBy;
			private _tarGroup = _enemyGroups#0;
			private _tarPos = getPosWorld leader _tarGroup;
			{
				//
				private _fire = false;
				if (_x#5 == SUPPORT_CAS) then {
					_fire = true;
				} else {
					private _usage = ((getText (configfile >> "CfgAmmo" >> _x#6 >> "aiAmmoUsageFlags")) splitString " + ");
					private _vehicle = ([_tarGroup] call ACF_getGroupVehicles) param [0, objNull];
					if (isNull _vehicle) then {
						if ("64" in _usage) then {
							_fire = true;
						};
					} else {
						if (_vehicle isKindOf "Tank") then {
							if ("512" in _usage) then {
								_fire = true;
							};
						} else {
							if ("128" in _usage) then {
								_fire = true;
							};
						};
					};
				};
				//
				if (random 7 <= 1 && {_fire} && {time >= _x#3}) then {
					//systemChat ("Preparation artillery fire on: " + GVAR(_targetBase,"callsign"));
					[_tarPos,_battalion,_forEachIndex] remoteExec ["ACF_callSupport",2];
				};
			} forEach GVARS(_battalion,"ec_supportsList",[]);
		};
		//systemChat str ["STR", _strength, _requiredStrength, count _groups];
	};
};

#define AS_STAGING 	1
#define AS_ATTACK 	2

// Info about offensive is stored by the agent
ACF_ai_offensiveAgent = {
	params ["_base","_side","_attackGroups"];
	private _battalion = [_side] call ACF_battalion;
	private _stagingDistance = OFFENSIVE_STAGING_DISTANCE + GVAR(_base,"out_perimeter");
	private _baseAttackDistance =  _stagingDistance * 1.25 + 50;

	private _offensiveName = "Offensive " + GVAR(_base,"callsign") + ": ";
	//systemChat (_offensiveName + "Offensive started");

	// 1. STAGING PHASE
	// Order all units to move to staging area
	SVAR(_base,"attackStage",AS_STAGING);
	{
		//SVAR(_x,"canGetOrders",false);
		[_x, _base, B_TRANSPORT, false, 5] call ACF_ai_moveToStaging;
	} forEach _attackGroups;
	if !(IS_AI_ENABLED(_battalion)) exitWith {};
	// Wait until initial timeout, or all units are ready to attack
	private _stagingTimeout = time + 300; // 5 minutes are basic timeout
	waitUntil {
		sleep STRATEGY_TICK;
		_attackGroups findIf {leader _x distance _base > _baseAttackDistance} == -1	|| {time > _stagingTimeout}
	};
	if !(IS_AI_ENABLED(_battalion)) exitWith {};
	_attackGroups = [_base,_battalion,_side,_attackGroups] call ACF_ai_attackReinforcements;

	//if (time > _stagingTimeout) then {
		//systemChat (_offensiveName + "Attacking after timeout");
	//} else {
		//systemChat (_offensiveName + "Attacking: all units ready");
	//};

	// 2. START ATTACK

	//determine defender detection type
	private _color = switch (_side) do {
	    case west: { "Wdetected" };
	    case east: { "Edetected" };
	    case resistance: { "Idetected" };
	};

	//use supports
	private _enemyGroups = AC_operationGroups select {side _x != _side && {GVARS(_x,_color,false)} };
	if (count _enemyGroups > 0) then {
		_enemyGroups = [_enemyGroups,[],{leader _x distance2D _base},"ASCEND"] call BIS_fnc_sortBy;
		private _tarGroup = _enemyGroups#0;
		private _tarPos = getPosWorld leader _tarGroup;
		{
			//
			private _fire = false;
			if (_x#5 == SUPPORT_CAS) then {
				_fire = true;
			} else {
				private _usage = ((getText (configfile >> "CfgAmmo" >> _x#6 >> "aiAmmoUsageFlags")) splitString " + ");
				private _vehicle = ([_tarGroup] call ACF_getGroupVehicles) param [0, objNull];
				if (isNull _vehicle) then {
					if ("64" in _usage) then {
						_fire = true;
					};
				} else {
					if (_vehicle isKindOf "Tank") then {
						if ("512" in _usage) then {
							_fire = true;
						};
					} else {
						if ("128" in _usage) then {
							_fire = true;
						};
					};
				};
			};
			//
			if (random 7 <= 1 && {_fire} && {time >= _x#3}) then {
				//systemChat ("Preparation artillery fire on: " + GVAR(_base,"callsign"));
				[_tarPos,_battalion,_forEachIndex] remoteExec ["ACF_callSupport",2];
			};
		} forEach GVARS(_battalion,"ec_supportsList",[]);
	};

	// Use artillery units
	if (GVAR(_base,"def_currentDetStr") > 5) then {
		{
			if !(IS_AI_ENABLED(_battalion)) exitWith {}; // add check for Resources
			if (random 3 <= 1) then {
				private _pos = [getPosWorld _base, GVAR(_base,"out_perimeter") / 2] call ACF_randomPos;
				[_x, _pos] call AC_ai_fireMission;
				//systemChat (_offensiveName + "Fire mission called");
			};
			sleep 3;
		} forEach GVARS(_battalion,"fireSupport",[]);
	};

	sleep STRATEGY_TICK;
	if !(IS_AI_ENABLED(_battalion)) exitWith {};

	// Order all attacking units to engage
	{
		[_x, _base, B_COMBAT, false, 5] call ACF_ai_moveToBase;
	} forEach _attackGroups;
	SVAR(_base,"attackStage",AS_ATTACK);

	sleep STRATEGY_TICK;
	_attackGroups = [_base,_battalion,_side,_attackGroups] call ACF_ai_attackReinforcements;

	// Check ending conditions
	private _ended = false;
	while {!_ended} do {
		sleep STRATEGY_TICK;
		if !(IS_AI_ENABLED(_battalion)) exitWith {};

		//Use strategy system evaluations for performance and consistency

		private _attDefRatio = (GVAR(_base,"thr_currentStr") max 0.01) / (GVAR(_base,"def_currentDetStr") max 0.01);
		SVAR(_base,"adRatio",_attDefRatio);
		//systemChat (_offensiveName + "A/D: " + str _attDefRatio);

		// Victory condition
		if (GVAR(_base,"side") == _side) then {
			_ended = true;
			//systemChat (_offensiveName + "Victory");
		} else {
			// Retreat condition
			if (_attDefRatio <= AD_RETREAT_THRESHOLD) then {
				{
					[_x, [_x] call ACF_rtbPos,B_TRANSPORT,true] call ACF_ai_move;
				} forEach _attackGroups;
				_ended = true;
				//systemChat (_offensiveName + "Retreat");
			};
		};

		if (!_ended) then {_attackGroups = [_base,_battalion,_side,_attackGroups] call ACF_ai_attackReinforcements;};

		// If attack is not ending, make sure no group is stuck on one place
		{
			if (leader _x distance _base < _baseAttackDistance) then {
				private _wp = [_x, _base, B_COMBAT, false, 5] call ACF_ai_moveToBase;
				sleep 1;
				//_wp setWaypointType "SAD";
				//systemChat format ["%1 replanning their attack waypoint",GVAR(_x,"callsign")];
			};
		} foreach _attackGroups;
	};

	// 3. End the attack, cleanup everything
	{
		SVAR(_x,"canGetOrders",true);
	} forEach _attackGroups;
	SVAR(_battalion,"attacks",GVAR(_battalion,"attacks") - [_base]);
};

// Perform AI move to staging area
ACF_ai_moveToStaging = {
	params ["_group","_base",["_moveType",B_DEFAULT],"_canGetOrders",["_randomization",0]];
	private _leaderPos = getPosWorld leader _group;
	private _distance = (GVAR(_base,"out_perimeter") + OFFENSIVE_STAGING_DISTANCE) * 0.8;
	private _stagingPoint = _base getRelPos [_distance,_base getRelDir _leaderPos];
	private _roads = _stagingPoint nearRoads 75;
	if (count _roads > 0) then
	{
		_roads = [_roads,[],{_x distance _base},"DESCEND"] call BIS_fnc_sortBy;
		private _road = _roads#0;
		_stagingPoint = getPosWorld _road;
	} else {
		//_stagingPoint = [_stagingPoint, 0, 45, 9, 0, 0.65, 0,[],[_stagingPoint,_stagingPoint]] call BIS_fnc_findSafePos;
		//_stagingPoint = _stagingPoint findEmptyPosition [0,45];
	};

	debugLog str _distance;

	[_x, _stagingPoint, _moveType, _canGetOrders, _randomization] call ACF_ai_move;
};

// Perform AI move to staging area
ACF_ai_moveToBase = {
	params ["_group","_base",["_moveType",B_DEFAULT],"_canGetOrders",["_randomization",0]];
	private _leaderPos = getPosWorld leader _group;
	private _distance = GVAR(_base,"out_perimeter") * 0.5;
	private _stagingPoint = _base getRelPos [_distance,_base getRelDir _leaderPos];
	private _roads = _stagingPoint nearRoads 30;
	if (count _roads > 0) then
	{
		_roads = [_roads,[],{_x distance _stagingPoint},"ASCEND"] call BIS_fnc_sortBy;
		private _road = _roads#0;
		_stagingPoint = getPosWorld _road;
	} else {
		//_stagingPoint = [_stagingPoint, 0, 30, 9, 0, 0.75, 0,[],[_stagingPoint,_stagingPoint]] call BIS_fnc_findSafePos;
		//_stagingPoint = _stagingPoint findEmptyPosition [0,30];
	};

	debugLog str _distance;

	[_x, _stagingPoint, _moveType, _canGetOrders, _randomization] call ACF_ai_move;
};

ACF_ai_attackReinforcements = {
	params ["_base","_battalion","_side","_currentAttackers"];

	private _idealAttackStr = (GVAR(_base,"att_costDet")) max 9;
	//private _maxAttackStr = 60 min ((_idealAttackStr * 1.5) max 20);

	//if(DEBUG_MODE) then {systemChat format ["%1 defense %2",GVAR(_base,"callsign"),_idealAttackStr]};
	// Check for all units you could put into attack

	private _hqGroup = GVARS(_battalion,"hqElement",grpNull);
	private _newAttackers = [];
	private _availableGroups = _currentAttackers + (AC_operationGroups select {
		side _x == _side
		&& {GVARS(_x,"canGetOrders",true)}
		&& {_x != _hqGroup}
		&& {GVAR(_x,"type") != TYPE_ARTILLERY}
	});
	_availableGroups = [_availableGroups,[],{leader _x distance2D _base},"ASCEND"] call BIS_fnc_sortBy;

	private _newStr = 0;

	// These should be hard-assigned units
	{
		//SVAR(_x,"canGetOrders",false);
		_newAttackers pushBackUnique _x;
		_newStr = _newStr + ([_x] call ACF_ai_groupStrength);

		if (_currentAttackers find _x == -1 || {count waypoints _x == 0}) then {
			if (GVAR(_base,"attackStage") == AS_STAGING) then {
				[_x, _base, B_TRANSPORT, false, 7] call ACF_ai_moveToStaging;
			} else {
				[_x, _base, B_COMBAT, false, 5] call ACF_ai_moveToBase;
			};
		};
		if (_newStr >= _idealAttackStr) exitWith {};
	} forEach _availableGroups;

	{
		SVAR(_x,"canGetOrders",true);
	} forEach (_currentAttackers - _newAttackers);

	//systemChat str [GVAR(_base,"callsign"), _newAttackers apply {GVAR(_x,"callsign")}];

	_newAttackers
};